import pandas as pd
import csv
from datetime import datetime as dt
from flask import Flask, request, render_template, url_for, redirect
import json
app = Flask(__name__)


def filter_movies(dict):
    var = pd.read_csv('information.csv')
    if dict['genres']:
        var = var[var.genres.str.contains(dict['genres'])]
    if dict['actor']:
        var = var[var.cast.str.contains(dict['actor'])]
    if dict['rating']:
        var = var[var.vote_average.between(int(dict['rating']), 11)]
    if dict['lang']:
        var = var[var.original_language == dict['lang']]
    if dict['fyear']:
        var.release_date = var.release_date.apply(lambda i: dt.strptime(i, "%Y-%m-%d"))
        var = var[var.release_date.dt.year.between(int(dict['fyear']), int(dict['tyear']))]

    return var[['title', 'overview']]

def get_cast():
    credits = pd.read_csv('information.csv')
    credits = credits.cast
    cast = set()
    for i in credits:
        i = json.loads(i)
        for j in i:
            cast.add(j["name"])
    return cast




@app.route('/', methods=('GET', 'POST'))
def hello():
    if request.method == 'POST':
        return redirect(url_for('hello1'))

    generes = ['History', 'Horror', 'Science Fiction', 'Fantasy', 'Music', 'Drama', 'Western', 'Family', 'Comedy',
               'Action', 'Animation', 'Adventure', 'Romance', 'Mystery', 'War', 'Foreign', 'Documentary', 'TV Movie',
               'Crime', 'Thriller']
    lan = {'ps':'Pushto', 'nb':'Norwegian Bokmål', 'pl':'Polish', 'ar':'Arabic'	, 'hi':'Hindi', 'te':'Telugu', 'ro':'Romanian', 'es':'Spanish', 'en':'English', 'vi':'Vietnamese', 'fa':'Persian', 'ru':'Russian', 'hu':'Hungarian', 'id':'Indonesian', 'ja':'Japanese', 'ko':'Korean', 'nl':'Dutch',
           'he':'Hebrew', 'tr':'Turkish', 'is':'Icelandic', 'zh':'Chinese', 'sl':'Slovenian', 'da':'Danish', 'no':'Norwegian', 'el':'Greek', 'cs':'Czech', 'de':'German', 'pt':'Portuguese', 'fr':'French', 'ta':'Tamil', 'it':'Italian', 'af':'Afrikaans', 'th':'Thai', 'ky':'Kirghiz',
           'sv':'Swedish'}
    cast = get_cast()
    return render_template('main_page.html', generes=generes, lan=lan, cast=cast)


@app.route('/noa1')
def hello1():
    return 'Ok!'


@app.route('/ok', methods=['POST'])
def handle_data():
    generes_ls = request.form.get('generes')
    fyear = request.form.get('fyear')
    tyear = request.form.get('tyear')
    lang = request.form.get('lan')
    rating = request.form.get('rating')
    actors = request.form.get('brow')

    users_input = {
        'genres': generes_ls,
        'fyear': fyear,
        'tyear': tyear,
        'actor': actors,
        'lang': lang,
        'rating': rating
    }
    df = filter_movies(users_input)

    return render_template('results.html', tables=[df.to_html(classes='wide', index=False)], titles=df.columns.values)
app.run()
