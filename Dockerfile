 FROM python:3.8
 WORKDIR /fangirl
 COPY . .
 RUN pip3 install csv && pip3 install pandas && pip3 install Flask && pip3 install json 
 CMD ["python", "app.py"]
